<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;

class UserController extends Controller
{

    public function register(Request $request)
    {
        $request->validate([
            'name' => 'required|string',
            'email' => 'required|string|unique:users,email',
            'role' => 'required',
            'faculty_id' => 'required',
            'password' => 'required|string|confirmed',
        ]);
        $user = new User();
        $user->name = $request->input('name');
        $user->email = $request->input('email');
        $user->role = $request->input('role');
        $user->faculty_id = $request->input('faculty_id');
        $user->email_verified_at = Carbon::now();
        $user->password = Hash::make($request->input('password'));
        $user->remember_token = Str::random(10);
        if ($user->save()) {
            $token = $user->createToken('scl-mgmt-token')->plainTextToken;

            $response = [
                'user' => $user,
                'token' => $token,
            ];

            return response($response, 201);
        }
        return ["error" => "user cannot be registered"];

    }

    public function login(Request $request)
    {
        $cred = $request->validate([
            'email' => 'required|exists:users,email',
            'password' => 'required',
        ]);
        if (Auth::attempt(['email' => $cred['email'], 'password' => $cred['password']])) {
            $token = Auth::user()->createToken('scl-mgmt-token')->plainTextToken;
            $response = [
                'message' => 'You are logged in',
                'token' => $token,
            ];
            return response($response, 200);
        }

        return ["error" => "could not login"];
    }

    public function updateinfo(Request $request)
    {
        $request->validate([
            'name' => 'required',
            'email' => 'required',
            'faculty_id' => 'required',
        ]);
        $data = User::find(Auth::user()->id);
        $data->name = $request->input('name');
        $data->email = $request->input('email');
        $data->faculty_id = $request->input('faculty_id');
        if ($data->save()) {
            $response = [
                'message' => 'your info updated',

            ];
            return response($response, 200);
        }
        return ["error" => "could not update the user data"];

    }

    public function logout()
    {
        Auth::user()->tokens()->delete();

        return [
            'message' => 'logged out',
        ];
    }
}