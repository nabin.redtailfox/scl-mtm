<?php

namespace App\Http\Controllers\Teacher;

use App\Http\Controllers\Controller;
use App\Models\Faculty;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;

class TeacherController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    public function index()
    {
        return view('dashboard.teachers.index');
    }

    public function profile()
    {

        return view('dashboard.teachers.profile');
    }

    public function profileEdit()
    {
        $id = Auth::user()->id;
        $editData = User::find($id);
        $faculty = Faculty::all();
        return view('dashboard.teachers.edit_profile', compact('editData', 'faculty'));
    }
    public function profileStore(Request $request)
    {
        $data = User::find(Auth::user()->id);
        $data->name = $request->name;
        $data->email = $request->email;
        $data->faculty_id = $request->faculty_id;
        if ($data->save()) {
            return redirect()->route('teacher.editprofile')->with('success', 'profile updated successfully');
        }
        return redirect()->route('teacher.editprofile')->with('error', 'something weent wrong, could  not update your profile');

    }

    public function settings()
    {
        return view('dashboard.teachers.settings');
    }

    public function changePassword(Request $request)
    {
        $request->validate([
            'current_password' => 'required',
            'password' => 'required|string|min:6|confirmed',
            'password_confirmation' => 'required',
        ]);

        $data = User::find(Auth::user()->id);

        if (!Hash::check($request->current_password, $data->password)) {
            return back()->with('error', 'Current password does not match!');
        }

        $data->password = Hash::make($request->password);
        $data->save();

        return back()->with('success', 'Password successfully changed!');

    }

}