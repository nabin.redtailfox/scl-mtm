<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

class UserController extends Controller
{
    public function userView()
    {
        // $allData = User::all();
        $data['allData'] = User::all()->where('role', '=', 3); // fetching data from multiple models
        return view('backend.user.view_user', $data);
    }

    public function userAdd()
    {
        return view('backend.user.add_user');
    }

    public function userStore(Request $request)
    {
        $request->validate([
            'name' => 'required',
            'email' => 'required|unique:users',
            'password' => 'required',

        ]);
        $data = new User();
        $data->name = $request->name;
        $data->email = $request->email;
        $data->role = 3;
        $data->password = Hash::make($request->password);

        if ($data->save()) {
            return redirect()->route('admin.userview')->with('success', 'Student is registered');
        }
        return redirect()->back()->with('error', "could not register the user");
    }

    public function userEdit($id)
    {
        $editData = User::find($id);
        return view('backend.user.edit_user', compact('editData'));
    }

    public function userUpdate(Request $request, $id)
    {
        $data = User::find($id);
        $data->name = $request->name;
        $data->email = $request->email;
        $data->role = 3;
        $data->password = Hash::make($request->password);

        if ($data->update()) {
            return redirect()->route('admin.userview')->with('success', 'Student info Updated');
        }
        return redirect()->back()->with('error', "could not update the user data");
    }

    public function userDelete($id)
    {
        $user = User::find($id);
        $user->delete();
        return redirect()->route('admin.userview')->with('success', 'User Deleted Successfully');
    }
}