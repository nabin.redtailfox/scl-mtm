<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

class TeacherController extends Controller
{
    public function teacherView()
    {
        // $allData = User::all();
        $data['allData'] = User::all()->where('role', '=', 2); // fetching data from multiple models
        return view('backend.teacher.view_teacher', $data);
    }

    public function teacherAdd()
    {
        return view('backend.teacher.add_teacher');
    }

    public function teacherStore(Request $request)
    {
        $request->validate([
            'name' => 'required',
            'email' => 'required|unique:users',
            'password' => 'required',

        ]);
        $data = new User();
        $data->name = $request->name;
        $data->email = $request->email;
        $data->role = 2;
        $data->password = Hash::make($request->password);

        if ($data->save()) {
            return redirect()->route('admin.teacherview')->with('success', 'teacher is registered');
        }
        return redirect()->back()->with('error', "could not register the user");
    }

    public function teacherEdit($id)
    {
        $editData = User::find($id);
        return view('backend.teacher.edit_teacher', compact('editData'));
    }

    public function teacherUpdate(Request $request, $id)
    {
        $data = User::find($id);
        $data->name = $request->name;
        $data->email = $request->email;
        $data->role = 2;
        $data->password = Hash::make($request->password);

        if ($data->update()) {
            return redirect()->route('admin.teacherview')->with('success', 'Teacher info Updated');
        }
        return redirect()->back()->with('error', "could not update the user data");
    }

    public function teacherDelete($id)
    {
        $user = User::find($id);
        $user->delete();
        return redirect()->route('admin.teacherview')->with('success', 'Teacher Deleted Successfully');
    }
}