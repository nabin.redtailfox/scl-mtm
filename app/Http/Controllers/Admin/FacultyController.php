<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Faculty;
use Illuminate\Http\Request;

class FacultyController extends Controller
{
    public function facultyView()
    {
        // $allData = User::all();
        $data['allData'] = Faculty::all(); // fetching data from multiple models
        return view('backend.faculty.view_faculty', $data);
    }

    public function facultyAdd()
    {
        return view('backend.faculty.add_faculty');
    }

    public function facultyStore(Request $request)
    {
        $request->validate([
            'name' => 'required',
        ]);
        $data = new Faculty();
        $data->name = $request->name;
        if ($data->save()) {
            return redirect()->route('admin.facultyview')->with('success', 'Faculty is Created');
        }
        return redirect()->back()->with('error', "could not create the faculty");
    }

    public function facultyEdit($id)
    {
        $editData = Faculty::find($id);
        return view('backend.faculty.edit_faculty', compact('editData'));
    }

    public function facultyUpdate(Request $request, $id)
    {
        $data = Faculty::find($id);
        $data->name = $request->name;

        if ($data->update()) {
            return redirect()->route('admin.facultyview')->with('success', 'Student info Updated');
        }
        return redirect()->back()->with('error', "could not update the user data");
    }

    public function facultyDelete($id)
    {
        $user = Faculty::find($id);
        $user->delete();
        return redirect()->route('admin.facultyview')->with('success', 'User Deleted Successfully');
    }
}