@extends('dashboard.admins.admin_master')
@section('admin-content')
<div class="content-wrapper">
    <div class="container-full">


        <section class="content">

            <!-- Basic Forms -->
            <div class="box">
                <div class="box-header with-border">
                    <h4 class="box-title">Update Student Info</h4>

                </div>
                <!-- /.box-header -->
                <div class="box-body">
                    <div class="row">
                        <div class="col">
                            <form method="POST" action="{{ route('admin.userupdate',$editData->id) }}">
                                @csrf
                                <div class="row">
                                    <div class="col-12">
                                        <div class="form-group">
                                            <h5>Name<span class="text-danger">*</span></h5>
                                            <div class="controls">
                                                <input type="text" name="name" id="name" class="form-control"
                                                    data-validation-required-message="This field is required"
                                                    value="{{ $editData->name }}">
                                                <div class="help-block"></div>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <h5>Email Field <span class="text-danger">*</span></h5>
                                            <div class="controls">
                                                <input type="email" name="email" id="email" class="form-control"
                                                    data-validation-required-message="This field is required"
                                                    value={{ $editData->email }}>
                                                <div class="help-block"></div>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <h5>Password Input Field <span class="text-danger">*</span></h5>
                                            <div class="controls">
                                                <input type="password" name="password" class="form-control"
                                                    data-validation-required-message="This field is required">
                                                <div class="help-block"></div>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <h5>Repeat Password Input Field <span class="text-danger">*</span></h5>
                                            <div class="controls">
                                                <input type="password" name="password_confirmation"
                                                    id="password-confirm" data-validation-match-match="password"
                                                    class="form-control">
                                                <div class="help-block"></div>
                                            </div>
                                        </div>


                                        {{-- <div class="form-group">
                                            <h5>Student <span class="text-danger">*</span></h5>
                                            <div class="controls">
                                                <input type="hidden" name="role" value="3">
                                                <div class="help-block"></div>
                                            </div>
                                        </div> --}}
                                        <div class="text-xs-right">
                                            <input type="submit" class="btn btn--rounded btn-info mb-5" value="update">
                                        </div>
                            </form>

                        </div>
                        <!-- /.col -->
                    </div>
                    <!-- /.row -->
                </div>
                <!-- /.box-body -->
            </div>
            <!-- /.box -->

        </section>


    </div>
</div>
@endsection
