@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{ __('Student Profile') }}</div>
                <div class="card-body">
                    @if (session('status'))
                    <div class="alert alert-success" role="alert">
                        {{ session('status') }}
                    </div>
                    @endif

                    <div class="box box-widget widget-user">
                        <!-- Add the bg color to the header using any of the bg-* classes -->
                        <div class="widget-user-header bg-black"
                            style="background: url('../images/gallery/full/10.jpg') center center;">
                            <h3 class="widget-user-username">{{ Auth::user()->name }}</h3>

                            <a href="{{ route('user.editprofile') }}" style="float: right"
                                class="btn btn-rounded btn-success mb-5">Edit Profile</a>
                        </div>
                        <div class="widget-user-image">
                            <img class="rounded-circle" src="{{ asset('backend/images/user3-128x128.jpg') }}"
                                alt="User Avatar">
                        </div>
                        <div class="box-footer">
                            <div class="row">
                                <div class="col-sm-4">
                                    <div class="description-block">
                                        <h5 class="description-header">User Type</h5>
                                        <span class="description-text">Student</span>
                                    </div>
                                    <!-- /.description-block -->
                                </div>
                                <!-- /.col -->
                                <div class="col-sm-4 br-1 bl-1">
                                    <div class="description-block">
                                        <h5 class="description-header">Email</h5>
                                        <span class="description-text">{{ Auth::user()->email }}</span>
                                    </div>
                                    <!-- /.description-block -->
                                </div>
                                <!-- /.col -->
                                <div class="col-sm-4">
                                    <div class="description-block">
                                        <h5 class="description-header">Faculty Assigned</h5>
                                        <span class="description-text">{{ Auth::user()->faculty->name }}</span>
                                    </div>
                                    <!-- /.description-block -->
                                </div>
                                <!-- /.col -->
                            </div>
                            <!-- /.row -->
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
