@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{ __('Edit Profile') }}</div>

                <div class="card-body">
                    @if (session('success'))
                    <div class="alert alert-success" role="alert">
                        {{ session('success') }}
                    </div>
                    @endif
                    @if (session('error'))
                    <div class="alert alert-success" role="alert">
                        {{ session('error') }}
                    </div>
                    @endif
                    <div class="row">
                        <div class="col">
                            <form method="POST" action="{{ route('teacher.profilestore') }}">
                                @csrf
                                <div class="row">
                                    <div class="col-12">
                                        <div class="form-group">
                                            <h5>Name<span class="text-danger">*</span></h5>
                                            <div class="controls">
                                                <input type="text" name="name" id="name" class="form-control"
                                                    data-validation-required-message="This field is required"
                                                    value="{{ $editData->name }}">
                                                <div class="help-block"></div>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <h5>Email Field <span class="text-danger">*</span></h5>
                                            <div class="controls">
                                                <input type="email" name="email" id="email" class="form-control"
                                                    data-validation-required-message="This field is required"
                                                    value={{ $editData->email }}>
                                                <div class="help-block"></div>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <h5>Faculty : ({{ $editData->faculty->name }}) <span
                                                    class="text-danger">*</span></h5>
                                            <div class="controls">
                                                <select name="faculty_id" id="select" required="" class="form-control">

                                                    <option value="" selected>
                                                        select one</option>
                                                    @foreach ( $faculty as $fac)

                                                    <option value="{{ $fac->id }}">{{ $fac->name }}</option>
                                                    @endforeach


                                                </select>
                                                <div class="help-block"></div>
                                            </div>
                                        </div>
                                        <div class="text-xs-right">
                                            <input type="submit" class="btn btn--rounded btn-info mb-5" value="update">
                                        </div>
                            </form>

                        </div>
                        <!-- /.col -->
                    </div>
                    <!-- /.row -->
                </div>
            </div>
        </div>
    </div>
</div>
</div>
@endsection
