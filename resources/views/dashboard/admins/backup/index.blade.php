@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{ __('Admin Dashboard') }}</div>

                <div class="card-body">
                    <h4>Hi Admin: {{ Auth::user()->name }}</h4>
                    <hr>
                    @if (session('status'))
                    <div class="alert alert-success" role="alert">
                        {{ session('status') }}
                    </div>
                    @endif

                    {{ __('You are logged in!') }}
                </div>
            </div>
        </div>
    </div>
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{ __('User management') }}</div>
                <ul>
                    <li><a href=""></a>students</li>
                    <li><a href=""></a>teachers</li>
                </ul>
                <div class="card-body">

                </div>
            </div>
        </div>
    </div>
</div>
@endsection
