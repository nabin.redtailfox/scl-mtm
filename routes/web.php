<?php

use App\Http\Controllers\Admin\AdminController;
use App\Http\Controllers\Admin\FacultyController;
use App\Http\Controllers\Admin\TeacherController as AdminTeacherController;
use App\Http\Controllers\Admin\UserController as AdminUserController;
use App\Http\Controllers\Teacher\TeacherController;
use App\Http\Controllers\User\UserController;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
 */

Route::get('/', function () {
    if (Auth::check()) {
        return redirect()->back()->with('status', 'you are already logged in');
    } else {
        return view('auth.login');
    }

});

//Middleware group
Route::middleware(['middleware' => 'PreventBackHistory'])->group(function () {
    Auth::routes();
});

// Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');

//Admin
Route::group(['namespace' => 'Admin', 'prefix' => 'admin', 'as' => 'admin.', 'middleware' => ['isAdmin', 'auth', 'PreventBackHistory']], function () {
    Route::get('dashboard', [AdminController::class, 'index'])->name('dashboard');
    Route::get('profile', [AdminController::class, 'profile'])->name('profile');
    Route::get('settings', [AdminController::class, 'settings'])->name('settings');

    //Users CRUD -admin dashboard
    Route::get('/user/view', [AdminUserController::class, 'userView'])->name('userview');
    Route::get('/user/add', [AdminUserController::class, 'userAdd'])->name('useradd');
    Route::post('/user/store', [AdminUserController::class, 'userStore'])->name('userstore');
    Route::get('/user/edit/{id}', [AdminUserController::class, 'userEdit'])->name('useredit');
    Route::post('/user/update/{id}', [AdminUserController::class, 'userUpdate'])->name('userupdate');
    Route::get('/user/delete/{id}', [AdminUserController::class, 'userDelete'])->name('userdelete');

    //Teachers CRUD -admin-dashboard
    Route::get('/teacher/view', [AdminTeacherController::class, 'teacherView'])->name('teacherview');
    Route::get('/teacher/add', [AdminTeacherController::class, 'teacherAdd'])->name('teacheradd');
    Route::post('/teacher/store', [AdminTeacherController::class, 'teacherStore'])->name('teacherstore');
    Route::get('/teacher/edit/{id}', [AdminTeacherController::class, 'teacherEdit'])->name('teacheredit');
    Route::post('/teacher/update/{id}', [AdminTeacherController::class, 'teacherUpdate'])->name('teacherupdate');
    Route::get('/teacher/delete/{id}', [AdminTeacherController::class, 'teacherDelete'])->name('teacherdelete');

    //Faculty CRUD -admin-dashboard
    Route::get('/faculty/view', [FacultyController::class, 'facultyView'])->name('facultyview');
    Route::get('/faculty/add', [FacultyController::class, 'facultyAdd'])->name('facultyadd');
    Route::post('/faculty/store', [FacultyController::class, 'facultyStore'])->name('facultystore');
    Route::get('/faculty/edit/{id}', [FacultyController::class, 'facultyEdit'])->name('facultyedit');
    Route::post('/faculty/update/{id}', [FacultyController::class, 'facultyUpdate'])->name('facultyupdate');
    Route::get('/faculty/delete/{id}', [FacultyController::class, 'facultyDelete'])->name('facultydelete');
});

//Teacher
Route::group(['namespace' => 'Teacher', 'prefix' => 'teacher', 'as' => 'teacher.', 'middleware' => ['isTeacher', 'auth', 'PreventBackHistory']], function () {
    //profile
    Route::get('profile', [TeacherController::class, 'profile'])->name('profile');
    Route::get('/profile/edit', [TeacherController::class, 'profileEdit'])->name('editprofile');
    Route::post('/profile/store', [TeacherController::class, 'profileStore'])->name('profilestore');
    //settings
    Route::get('settings', [TeacherController::class, 'settings'])->name('settings');
    Route::post('settings/changepassword', [TeacherController::class, 'changePassword'])->name('profilesettings');
});

//User
Route::group(['namespace' => 'User', 'prefix' => 'user', 'as' => 'user.', 'middleware' => ['isUser', 'auth', 'PreventBackHistory']], function () {
    //profile
    Route::get('profile', [UserController::class, 'profile'])->name('profile');
    Route::get('/profile/edit', [UserController::class, 'profileEdit'])->name('editprofile');
    Route::post('/profile/store', [UserController::class, 'profileStore'])->name('profilestore');
    //settings
    Route::get('settings', [UserController::class, 'settings'])->name('settings');
    Route::post('settings/changepassword', [UserController::class, 'changePassword'])->name('profilesettings');

});