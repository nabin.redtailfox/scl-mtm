<?php

use App\Http\Controllers\Api\UserController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
 */

// Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
//     return $request->user();
// });

Route::post('/register', [UserController::class, 'register']);
Route::post('/login', [UserController::class, 'login']);
//protected routes
Route::group(['namespace' => 'Api', 'middleware' => ['auth:sanctum', 'isUser']], function () {
    Route::get('userslist', [UserController::class, 'userslist']);
    Route::post('logout', [UserController::class, 'logout']);
    Route::put('update', [UserController::class, 'updateinfo']);
    Route::put('delete', [UserController::class, 'deleteinfo']);

});
